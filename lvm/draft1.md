Image of the low disk space warning.

Uh oh, Gnome just said the home volume is almost out of space! Luckily, there is probably
some space sitting in another volume, unused and ready to be relocated. This article will
show you how.

The key technology to making this relatively easy is because Fedora uses a hard-drive
partitioning tool called Logical Volume Manager (LVM). This incredibly flexible tool has
all kinds of features, from striping or mirroring multiple drives, to using a high-speed
drive as a cache for a slower drive, and providing fast read-write volume snapshotting.
Luckily to reclaim a bit of space, only the basics are needed.

There are a few key terms that need to be defined in order to understand what is going on:

Volume Group

This is the main container in the LVM system. There can be one, or many, volume groups in
a single machine, though a single volume group is the most common (and the default
configuration for Fedora systems).

Physical Volume

Physical volumes are added to a volume group in order to provide actual physical storage
for the volume group to use. This is the first key to the power of LVM. A physical volume
can be an entire hard-drive, or a partition on a hard-drive and there can be any number of
physical volumes scattered across any number of physical devices.

Logical Volume

Logical volumes are where file-systems are created, and then mounted into the system as
directories to be used. These are similar to hard-drive partitions, but are so much more
flexible.

By separating the physical representation from the logical one, LVM unlocks a lot of
flexibility. File-systems (residing on a logical volume) can move between physical
devices, or span multiple devices. Logical volumes are allocated from the "pool" of
storage provided by all of the physical volumes in the system.

WARNING Make sure all data is backed up!!

Following this guide *should* not result in any data loss, but it is mucking with some
very low-level and powerful commands. One mistake could cause the loss of all data on the
hard-drive.

Requirements

Free space must be available in one of the other volumes. Without extra space, there is
nothing to reclaim. Use the *gnome-disks* application to confirm one of the other volumes
has available space.

A Fedora live CD/USB is needed for this fix, because any file-system that needs to shrink
must be unmounted. Running from a live CD allows all the volumes on the hard-disk to
remain unmounted.

To begin, boot the Fedora live CD and select *Try Fedora*. Next, launch *gnome-disks* by
using the *Run Command* dialog (accessible by pressing *Alt-F2*). Find the full volume by
selecting from the list to left, then click on the *play* icon to mount it. Confirm the
correct volume is selected by looking at the usage. Here the volume is *home* in the
*fedora_localhost-live* volume group and is 94% full.

[gnome disks image]

Unmount the volume by clicking the *stop* button. Now, find a volume with available space
by selecting each one and inspecting the current usage (mounting & unmounting). Take
careful note of how much space is left in the chosen volume, and how much is available. Do
not take all the available space, otherwise that volume will be the next one to fill up!
Don't worry if the volume is not shown "next to" the full volume, unlike partitions,
logical volumes have no specific order.

All the preparation is complete, now it's time to resize some volumes! *gnome-disks* is a
friendly application, but it doesn't have the necessary power to shrink the filesystem.
For that, launch *blivet-gui* using the *Run Command* (*Alt-F2*). Choose the volume group
under LVM on the left, and find the volume with extra space. Select the volume, click on
the *gears* to edit the device, and select *resize*.

[blivet-gui with selected device]

Set the new size for the volume based on the available space discovered using
*gnome-disks* earlier. **Be careful.** If the new size is smaller than the data on the
disk, data loss will occur. Once the resize is complete, a new *free space* row should
appear. Now select the volume that needs to grow and navigate to the resize dialog again.
This time, set the device size to the max available. *blivet-gui* automatically calculated
the available space.

At this point, nothing has actually happened to the disk. To actually make the changes to
the disk, select the *check* in the top-right of *blivet-gui*. Review the summary of the
changes, and if everything looks right, click *Ok* to proceed.

Success! Once *blivet-gui* finishes, reboot back into the main Fedora install and enjoy
the extra room in the previously full volume!

Conclusion

Easily moving space between value just scratches the surface of the feature provided by
LVM. Most people, especially on the desktop, don't need the more advanced features.
However, when the need arises LVM is there, even if it can be a bit complex. Starting with
Fedora 33 the default filesystem for new installs is BTRFS. BTRFS is a easier to manage
while still providing a lot of flexibility. Check out the recent Fedora Magazine articles
covering BTRFS to learn more.
