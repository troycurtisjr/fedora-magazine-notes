Reclaim hard-drive space with LVM

[[Image of low disk warning]]

Uh oh, Gnome just said the home volume is almost out of space! Luckily, there is likely some space sitting around in another volume, unused and ready to relocate. Here's how to reclaim some of that space and put to use in the volume that needs it.

The key to easily relocate space between volumes is the Logical Volume Manager (LVM). Fedora 32 and before use LVM to divide disk space by default. This technology is similar to standard hard-drive partitions, but is a lot more flexible. LVM enables not only flexible volume size management, but also advanced capabilities such as read-write snapshots, striping or mirroring data across multiple drives, and using a high-speed drive as a cache for a slower drive. All of the options can get a bit detailed, but resizing a volume is straight-forward.

The *volume group* services as the main container in the LVM system. By default Fedora only defines a single volume group, but there can be as many as needed. Actual hard-drive and hard-drive partitions are added to the *volume group* as *physical volumes*. *Physical volumes* add available free space to the *volume group*. A typical Fedora install has one formatted boot partition, and the rest of the drive is a partition configured as an LVM *physical volume*. Out of this pool of available space, the *volume group* allocates one or more *logical volumes*. These volumes are similar to hard-drive partitions, but without the limitation of contiguous space on the disk. LVM *logical volumes* are not even limited to a single device! These *logical volumes* are given a size, formatted with any desired file system, and mounted to specific directories in the same way as hard-drive partitions.

## What's needed

Confirm the system uses LVM with the *gnome-disks* application, and make sure there is free space available in other volumes. Without space to reclaim from another volume, this guide isn't useful. A [Fedora live CD/USB](https://getfedora.org/en/workstation/download/) is also needed. Any file system that needs to shrink must be unmounted. Running from a live image allows all the volumes on hard-disk to remain unmounted, even important directories like */* and */home*.

[[image of gnome-disks?]]

## WARNING

No data should be lost by following this guide, but it is mucking around with some very low-level and powerful commands. One mistake could destroy all data on the hard-drive. So backup all the data on the disk!

## Resizing volumes

To begin, boot the Fedora live image and select *Try Fedora* at the dialog. Next, use the *Run Command* to launch the *blivet-gui* application (accessible by pressing *Alt-F2*, typing *blivet-gui*, then pressing *enter*). Select the *volume group* on the left under *LVM*. The current LVM logical volumes are displayed on the right.

[[image of blivet-gui]]

The *logical volume* labels consist of both the *volume group* name and the *logical volume* name. In the example, the *volume group* is "fedora_localhost-live" and there are "home", "root", and "swap" logical volumes allocated. To find the full volume, select each one, click on the *gear* icon, and choose *resize*. The slider in the resize dialog indicates the allowable sizes for the volume. The minimum value on the left is the space already in use within the file system, so this is the minimum possible volume size (without deleting data). The maximum value on the right is the greatest size the volume can have based on available free space in the *volume group*.

[[image of blivet-gui resize dialog]]

If the *resize* option is grayed out, that means the volume is full and the *volume group* has no available space. It's time to change that! Look through all of the volumes to find one with plenty of extra space, like in the screenshot above. Move the slider to the left to set the new size. Free up enough space to be useful for the full volume, but still leave plenty of space for future data growth. Otherwise, this volume will be the next to fill up.

Click *resize* and note that a new item appears in the volume listing: *free space*. Now select the full volume that started this whole endeavor, and move the slider all the way to the right. Press *resize* and marvel at the new improved volume layout. However, nothing has changed on the hard drive yet. Click on the *check-mark* to commit the changes to disk.

[[image of the confirmation dialog]]

Review the summary of the changes, and if everything looks right, click *Ok* to proceed. Once *blivet-gui* finishes, the work is done. Now reboot back into the main Fedora install and enjoy all the new space in the previously full volume.

## Wrap-up

Easy volume size management just scratches the surface of LVM capabilities. Most people, especially on the desktop, probably don't need the more advanced features. However, LVM is there when the need arises, even if it can get a bit complex to implement. Note that starting with Fedora 33, the default file system for new installs is BTRFS and LVM is not used. BTRFS can be easier to manage while still flexible enough for most common usages. Check out the recent Fedora Magazine articles on BTRFS to learn more.
